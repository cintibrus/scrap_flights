import pandas as pd
import numpy as np

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup

import matplotlib.pyplot as plt

import datetime
import logging as log
import time
import unicodedata

pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 10)
pd.set_option('display.width', 1000)

class GoogleFlights:

	def	__init__(self,csv_filename):
		self.csv_filename = csv_filename

	def get_driver(self):
		profile = webdriver.FirefoxProfile()
		profile.set_preference("general.useragent.override","'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'")
		profile.set_preference("javascript.enabled", True)
		profile.set_preference("media.peerconnection.enabled", False)
		profile.set_preference("media.navigator.permission.disabled", True)
		profile.set_preference("browser.cache.disk.enable", True)
		profile.set_preference("browser.cache.memory.enable", True)
		profile.set_preference("browser.cache.offline.enable", True)
		profile.set_preference("permissions.default.microphone", 1)
		profile.set_preference("permissions.default.camera", 1)
		profile.set_preference("dom.webnotifications.enabled", False)

		browser = webdriver.Firefox(profile)
		return browser

	def get_data(self,driver,url,div_class='YYoDtc'):
		driver.get('https://www.google.com')
		driver.get(url)
		delay = 2 # seconds
		try:
			myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, div_class)))
			print "Page is ready!"
		except Exception as e:
			print(e)
			print "Loading took too much time!"
			return pd.DataFrame()

		s = BeautifulSoup(driver.page_source, "lxml")
		try:
			collapse_tags = s.findAll('div', 'gws-flights-results__collapsed-itinerary gws-flights-results__itinerary')
			best_price_tags = [x.find('div', {"class": 'gws-flights-results__price'}) for x in collapse_tags]
			duration_tags = [x.find('div', {"class": 'gws-flights-results__duration'}) for x in collapse_tags]
			stops_tags = [x.find('div', {"class": 'gws-flights-results__stops'}) for x in collapse_tags]
			times_tags = [x.find('div', {"class": 'gws-flights-results__times'}) for x in collapse_tags]
			carriers_tags = [x.find('div', {"class": 'gws-flights-results__carriers'}) for x in collapse_tags]
			print best_price_tags
			prices = []
			durations = []
			stops = []
			times=[]
			carriers = []
			for tag0,tag1,tag2,tag3,tag4 in zip(best_price_tags,duration_tags,stops_tags,times_tags,carriers_tags):
				prices.append(tag0.text.replace('$', '').replace(',', ''))
				durations.append(tag1.text)
				stops.append(tag2.text)
				times.append(tag3.text)
				carriers.append(tag4.text)
			df = pd.DataFrame(
				{'price': prices,
				 'duration': durations,
				 'stops': stops,
				 'times': times,
				 'carriers': carriers
				 })
			return df
		except Exception as e:
			print(e)
			return pd.DataFrame()

	def get_data_from_map(self,driver,url,div_class='We9iXe'):
		try:
			driver.get('https://www.google.com')
			driver.get(url)
			delay = 2 # seconds
			myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CLASS_NAME, div_class)))
			print "Page is ready!"
		except Exception as e:
			print(e)
			print "Loading took too much time!"
			return pd.DataFrame()
		try:
			s = BeautifulSoup(driver.page_source, "lxml")
			prices = []
			durations = []
			stops = []
			cities=[]
			collapse_tags = s.findAll('div', {"jsname":'destinationCard'})
			print collapse_tags
			for x in collapse_tags:
				try:
					if x.find('div', {"class": 'uKOpFp4SF2X__price-row'}).text:
						print 'price: ',x.find('div', {"class": 'uKOpFp4SF2X__price-row'}).text,' duration: ',x.find('span', {"class": 'uKOpFp4SF2X__duration'}).text,' stops: ',x.find('span', {"class": 'gws-flights__flex-shrink gws-flights__ellipsize'}).text, ' cities: ',x.find('h3', {"class": 'flt-subhead1'}).text
						prices.append(x.find('div', {"class": 'uKOpFp4SF2X__price-row'}).text.replace('$', '').replace(',', ''))
						durations.append(x.find('span', {"class": 'uKOpFp4SF2X__duration'}).text)
						stops.append(x.find('span', {"class": 'gws-flights__flex-shrink gws-flights__ellipsize'}).text)
						cities.append(x.find('h3', {"class": 'flt-subhead1'}).text)
				except Exception as e:
					print(e)
			df = pd.DataFrame(
				{'price': prices,
				 'duration': durations,
				 'stops': stops,
				 'city': cities
				 })
			return df
		except Exception as e:
			print(e)
			return pd.DataFrame()

	def create_country_dataframe(self,driver,region,travel_lenght_list,start_period,end_period,steps_days,takeoff_after,takeoff_until,return_after,return_until,csv_filename,nr_passengers=2):
		# https://www.wikidata.org/wiki/Wikidata:WikiProject_Countries
		country_dict_test = {
			'Germany':'r/m/0345h',
		}
		country_dict_europe = {
			'United kingdom':'r/m/07ssc',
			'Albania':'r/m/0jdx',
			'Andorra':'r/m/0hg5',
			'Austria':'r/m/0h7x',
			'Belarus':'r/m/0163v',
			'Belgium':'r/m/0154j',
			'Bulgaria':'r/m/015qh',
			'Croatia':'r/m/01pj7',
			'Cyprus':'r/m/01ppq',
			'Denmark':'r/m/0k6nt',
			'Estonia':'r/m/02kmm',
			'Finland':'r/m/02vzc',
			'France':'r/m/0f8l9c',
			'Germany':'r/m/0345h',
			'Greece':'r/m/035qy',
			'Hungary':'r/m/03gj2',
			'Iceland':'r/m/03rj0',
			'Ireland':'r/m/03rt9',
			'Italy':'r/m/03rjj',
			'Latvia':'r/m/04g5k',
			'Liechtenstein':'r/m/04j53',
			'Lithuania':'r/m/04gzd',
			'Luxembourg':'r/m/04g61',
			'Macedonia':'r/m/0bjv6',
			'Malta':'r/m/04v3q',
			'Moldova':'r/m/04w4s',
			'Montenegro':'r/m/056vv',
			'Monaco':'r/m/04w58',
			'Norway':'r/m/05b4w',
			'Poland':'r/m/05qhw',
			'Portugal':'r/m/05r4w',
			'Romania':'r/m/06c1y',
			'Russia':'r/m/06bnz',
			'Serbia':'r/m/077qn',
			'Slovakia':'r/m/06npd',
			'Slovenia':'r/m/06t8v',
			'Spain':'r/m/06mkj',
			'Sweden':'r/m/0d0vqn',
			'Switzerland':'r/m/06mzp',
			'Turkey':'r/m/01znc_',
			'Ukraine':'r/m/07t21',
		}
		country_dict_asia={
			'Afghanistan':'r/m/0jdd',
			'Armenia':'r/m/0jgx',
			'Azerbaijan':'r/m/0jhd',
			'Bahrain':'r/m/0161c',
			'Bangladesh':'r/m/0162b',
			'Bhutan':'r/m/07bxhl',
			'Brunei':'r/m/0167v',
			'Cambodia':'r/m/01xbgx',
			'China':'r/m/0d05w3',
			'Cyprus':'r/m/01ppq',
			'Georgia':'r/m/0d0kn',
			'India':'r/m/03rk0',
			'Indonesia':'r/m/03ryn',
			'Iran':'r/m/03shp',
			'Iraq':'r/m/0d05q4',
			'Israel':'r/m/03spz',
			'Japan':'r/m/03_3d',
			'Jordan':'r/m/03__y',
			'Kazakhstan':'r/m/047lj',
			'Kuwait':'r/m/047yc',
			'Kyrgyzstan':'r/m/0jt3tjf',
			'Laos':'r/m/04hhv',
			'Lebanon':'r/m/04hqz',
			'Malaysia':'r/m/09pmkv',
			'Maldives':'r/m/04ty8',
			'Mongolia':'r/m/04w8f',
			'Myanmar':'r/m/04xn_',
			'Nepal':'r/m/016zwt',
			'Oman':'r/m/05l8y',
			'Pakistan':'r/m/05sb1',
			'Philippines':'r/m/05v8c',
			'Qatar':'r/m/0697s',
			'Singapore':'r/m/06t2t',
			'Syria':'r/m/06vbd',
			'Tajikistan':'r/m/07dvs',
			'Thailand':'r/m/07f1x',
			'Turkey':'r/m/01znc_',
			'Turkmenistan':'r/m/01c4pv',
			'Uzbekistan':'r/m/07t_x',
			'Vietnam':'r/m/01crd5',
			'Yemen':'r/m/01z88t',
		}
		country_dict_africa = {
			'Algeria':'r/m/0h3y',
			'Angola':'r/m/0j4b',
			'Benin':'r/m/0164v',
			'Botswana':'r/m/0166v',
			'Burundi':'r/m/0169t',
			'Cameroon':'r/m/01nln',
			'Chad':'r/m/01p1b',
			'Comoros':'r/m/01n6c',
			'Djibouti':'r/m/027jk',
			'Egypt':'r/m/02k54',
			'Eritrea':'r/m/02khs',
			'Ethiopia':'r/m/019pcs',
			'Gabon':'r/m/03548',
			'Gambia':'r/m/0hdx8',
			'Ghana':'r/m/035dk',
			'Guinea':'r/m/03676',
			'Kenya':'r/m/019rg5',
			'Lesotho':'r/m/04hvw',
			'Liberia':'r/m/04hzj',
			'Libya':'r/m/04gqr',
			'Madagascar':'r/m/04sj3',
			'Malawi':'r/m/04tr1',
			'Mali':'r/m/04v09',
			'Mauritania':'r/m/04vjh',
			'Mauritius':'r/m/04vs9',
			'Morocco':'r/m/04wgh',
			'Mozambique':'r/m/04wlh',
			'Namibia':'r/m/05bmq',
			'Niger':'r/m/05cc1',
			'Nigeria':'r/m/05cgv',
			'Rwanda':'r/m/06dfg',
			'Senegal':'r/m/06srk',
			'Seychelles':'r/m/06sw9',
			'Somalia':'r/m/06tgw',
			'Sudan':'r/m/06tw8',
			'Swaziland':'r/m/06v36',
			'Tanzania':'r/m/07dzf',
			'Togo':'r/m/07f5x',
			'Tunisia':'r/m/07fj_',
			'Uganda':'r/m/07tp2',
			'Zambia':'r/m/088vb',
			'Zimbabwe':'r/m/088q4',
		}
		country_dict_america = {
			'Bahamas':'r/m/0160w',
			'Barbados':'r/m/0162v',
			'Belize':'r/m/0164b',
			'Canada':'r/m/0d060g',
			'Cuba':'r/m/0d04z6',
			'Dominica':'r/m/027nb',
			'Grenada':'r/m/035yg',
			'Guatemala':'r/m/0345_',
			'Haiti':'r/m/03gyl',
			'Honduras':'r/m/03h2c',
			'Jamaica':'r/m/03_r3',
			'Mexico':'r/m/0b90_r',
			'Nicaragua':'r/m/05c74',
			'Panama':'r/m/05qx1',
			'Argentina':'r/m/0jgd',
			'Bolivia':'r/m/0165v',
			'Brazil':'r/m/015fr',
			'Chile':'r/m/01p1v',
			'Colombia':'r/m/01ls2',
			'Ecuador':'r/m/02k1b',
			'Guyana':'r/m/034m8',
			'Paraguay':'r/m/05v10',
			'Peru':'r/m/016wzw',
			'Suriname':'r/m/06nnj',
			'Uruguay':'r/m/07twz',
		}
		country_dict_oceania = {
			'Australia':'r/m/0chghy',
			'Fiji':'r/m/02wt0',
			'Kiribati':'r/m/047t_',
			'Nauru':'r/m/05br2',
			'Palau':'r/m/05tr7',
			'Samoa':'r/m/06s9y',
			'Tonga':'r/m/07fb6',
			'Tuvalu':'r/m/07fsv',
			'Vanuatu':'r/m/07z5n'
		}
		if (region=='europe'):
			country_dict = country_dict_europe
		if (region=='asia'):
			country_dict = country_dict_asia
		if (region=='africa'):
			country_dict = country_dict_africa
		if (region=='america'):
			country_dict = country_dict_america
		if (region=='oceania'):
			country_dict = country_dict_oceania
		if (region=='test'):
			country_dict = country_dict_test

		res = pd.DataFrame()
		for travel_lenght in travel_lenght_list:
			start_period_date = datetime.datetime.strptime(start_period, "%Y-%m-%d")
			end_period_date = datetime.datetime.strptime(end_period, "%Y-%m-%d")
			numdays = end_period_date-start_period_date
			numdays = numdays.days+1
			for first_date in [start_period_date + datetime.timedelta(days=x) for x in range(0,numdays,steps_days)]:
				first_date_str = first_date.strftime("%Y-%m-%d")
				second_date = first_date + datetime.timedelta(days=travel_lenght)
				second_date_str = second_date.strftime("%Y-%m-%d")
				for country in country_dict:
					print first_date_str,second_date_str,country
					write_to_csv=True
					city = country_dict[country]
					url = "https://www.google.com/flights/#flt=AMS.{city}.{first_date_str}*{city}.AMS.{second_date_str};c:EUR;e:1;px:{nr_passengers};sd:1;t:e;dt:{takeoff_after}-{takeoff_until}*{return_after}-{return_until}".format(nr_passengers=nr_passengers,first_date_str=first_date_str,second_date_str=second_date_str,city=city,takeoff_after=takeoff_after,takeoff_until=takeoff_until,return_after=return_after,return_until=return_until)
					print(url)
					attemps = 0
					df = pd.DataFrame()
					while (attemps<3)and(df.empty):
						df = pd.DataFrame()
						attemps=attemps+1
						df = self.get_data_from_map(driver,url)
					try:
						if not df.empty:
							print df.head(1)
							df['from']=first_date_str
							df['to']=second_date_str
							df['country']=country
							res = pd.concat([res,df],ignore_index=True)
							if (write_to_csv):
								print('Writing to csv '+csv_filename)
								write_to_csv=False
								res.to_csv(csv_filename, encoding = 'utf-8')
					except Exception as e:
						print(e)
						res = df
		return res

	def create_dataframe_worldwide(self,driver,travel_lenght_list,start_period,end_period,steps_days,takeoff_after,takeoff_until,return_after,return_until,csv_filename,max_budget,nr_passengers,city_code):
		res = pd.DataFrame()
		for travel_lenght in travel_lenght_list:
			start_period_date = datetime.datetime.strptime(start_period, "%Y-%m-%d")
			end_period_date = datetime.datetime.strptime(end_period, "%Y-%m-%d")
			numdays = end_period_date-start_period_date
			numdays = numdays.days+1
			for first_date in [start_period_date + datetime.timedelta(days=x) for x in range(0,numdays,steps_days)]:
				write_to_csv=True
				first_date_str = first_date.strftime("%Y-%m-%d")
				second_date = first_date + datetime.timedelta(days=travel_lenght)
				second_date_str = second_date.strftime("%Y-%m-%d")
				url = "https://www.google.com/flights?hl=en#flt={city_code}..{first_date_str}*.{city_code}.{second_date_str};c:EUR;e:1;dt:{takeoff_after}-{takeoff_until}*{return_after}-{return_until};ls:1w;p:{max_budget}.0.EUR;px:{nr_passengers};sd:1;er:-900000000.-1800000000.900000000.1800000000;t:e".format(city_code=city_code,nr_passengers=nr_passengers,first_date_str=first_date_str,second_date_str=second_date_str,takeoff_after=takeoff_after,takeoff_until=takeoff_until,return_after=return_after,return_until=return_until,max_budget=max_budget)
				print(url)
				attemps = 0
				df = pd.DataFrame()
				while (attemps<3)and(df.empty):
					df = pd.DataFrame()
					attemps=attemps+1
					df = self.get_data_from_map(driver,url,'uKOpFp4SF2X__card')
				try:
					if not df.empty:
						print df.head(1)
						df['from']=first_date_str
						df['to']=second_date_str
						res = pd.concat([res,df],ignore_index=True)
						if (write_to_csv):
							print('Writing to csv '+csv_filename)
							res.to_csv(csv_filename, encoding = 'utf-8')
				except Exception as e:
					print(e)
					res = df
		return res

	def create_dataframe(self,driver,city_list,travel_lenght_list,start_period,end_period,steps_days,takeoff_after,takeoff_until,return_after,return_until,csv_filename,nr_passengers=2):
		res = pd.DataFrame()
		for travel_lenght in travel_lenght_list:
			start_period_date = datetime.datetime.strptime(start_period, "%Y-%m-%d")
			end_period_date = datetime.datetime.strptime(end_period, "%Y-%m-%d")
			numdays = end_period_date-start_period_date
			numdays = numdays.days+1
			for first_date in [start_period_date + datetime.timedelta(days=x) for x in range(0,numdays,steps_days)]:
				write_to_csv=True
				first_date_str = first_date.strftime("%Y-%m-%d")
				second_date = first_date + datetime.timedelta(days=travel_lenght)
				second_date_str = second_date.strftime("%Y-%m-%d")
				for city in city_list:
					url = "https://www.google.com/flights/#flt=AMS.{city}.{first_date_str}*{city}.AMS.{second_date_str};c:EUR;e:1;px:{nr_passengers};sd:1;t:f;dt:{takeoff_after}-{takeoff_until}*{return_after}-{return_until}".format(nr_passengers=nr_passengers,first_date_str=first_date_str,second_date_str=second_date_str,city=city,takeoff_after=takeoff_after,takeoff_until=takeoff_until,return_after=return_after,return_until=return_until)
					print(url)
					attemps = 0
					df = pd.DataFrame()
					while (attemps<3)and(df.empty):
						df = pd.DataFrame()
						attemps=attemps+1
						df = self.get_data(driver,url)
					try:
						if not df.empty:
							print df.head(1)
							df['from']=first_date_str
							df['to']=second_date_str
							df['city']=city
							res = pd.concat([res,df],ignore_index=True)
							if (write_to_csv):
								print('Writing to csv '+csv_filename)
								write_to_csv=False
								res.to_csv(csv_filename, encoding = 'utf-8')
					except Exception as e:
						print(e)
						res = df
		return res

	def createDataFromCountry(self,region,travel_lenght_list,start_period,end_period,steps_days,takeoff_after="0000",takeoff_until="2400",return_after="0000",return_until="2400",nr_passengers=2):
		driver = self.get_driver()
		res = self.create_country_dataframe(driver,region,travel_lenght_list,start_period,end_period,steps_days,takeoff_after,takeoff_until,return_after,return_until,self.csv_filename,nr_passengers)
		print res.tail(500)
		print "Done!"
		driver.quit()

	def createDataWorldwide(self,travel_lenght_list,start_period,end_period,steps_days,takeoff_after="0000",takeoff_until="2400",return_after="0000",return_until="2400",max_budget=300,nr_passengers=2,city_code='AMS'):
		driver = self.get_driver()
		res = self.create_dataframe_worldwide(driver,travel_lenght_list,start_period,end_period,steps_days,takeoff_after,takeoff_until,return_after,return_until,self.csv_filename,max_budget,nr_passengers,city_code)
		print res.tail(500)
		print "Done!"
		driver.quit()

	def createData(self,city_list,travel_lenght_list,start_period,end_period,steps_days,takeoff_after="0000",takeoff_until="2400",return_after="0000",return_until="2400",nr_passengers=2):
		driver = self.get_driver()
		res = self.create_dataframe(driver,city_list,travel_lenght_list,start_period,end_period,steps_days,takeoff_after,takeoff_until,return_after,return_until,self.csv_filename,nr_passengers)
		print res.tail(500)
		driver.quit()

	def analyseCountryData(self,max_price=180,duration_max=24,stops_hour_max = 24,from_start='2019-08-24',from_end='2019-12-30',countries=None):
		df=pd.read_csv(self.csv_filename, encoding='utf-8')
		df['price_num'] = df['price'].str.replace(unicodedata.lookup('EURO SIGN'),'').apply(pd.to_numeric)
		df['duration_hour'] = df['duration'].str.extract(pat = '([0-9]*) h').apply(pd.to_numeric)
		df['stops'] = df['stops'].str.extract(pat = '([0-9]*) stop').apply(pd.to_numeric)
		df['stops'].fillna(0, inplace=True)
		df = df[df['duration_hour']<duration_max]
		df = df[df['stops']<3]
		df = df[df['price_num']<max_price]
		df = df[df['from']>from_start]
		df = df[df['from']<from_end]
		try:
			df = pd.merge(df, countries, on='city', how='left')
		except Exception, e:
			print(e)
		print df.columns
		print "Group by city"
		print df.loc[df.groupby("city")["price_num"].idxmin()].head(10)
		print "Group by country"
		print df.loc[df.groupby("country_x")["price_num"].idxmin()].sort_values(by=['price_num'], ascending=True)[['country_x','city','price_num','from','to']].head(15)
		print "Group by dates"
		print df.loc[df.groupby("from")["price_num"].idxmin()].sort_values(by=['from'], ascending=True)[['country_x','city','price_num','from','to']].head(20)
		print "Sort by price"
		print df.sort_values(by=['price_num'], ascending=True)[['country_x','city','price_num','from','to']].head(20)


	def analyseData(self,max_price=180,duration_max=24,flight_hour_max = 24,from_start='2019-08-24',from_end='2019-12-30',countries=None):
		df=pd.read_csv(self.csv_filename, encoding='utf-8')
		df['price_num'] = df['price'].str.replace(unicodedata.lookup('EURO SIGN'),'').apply(pd.to_numeric)
		df['duration_hour'] = df['duration'].str.extract(pat = '([0-9]*) h').apply(pd.to_numeric)
		df['flight_hour'] = df['times'].str.extract(pat = '([0-9]*):').apply(pd.to_numeric)

		df = df[df['duration_hour']<duration_max]
		df = df[df['flight_hour']<flight_hour_max]
		df = df[df['price_num']<max_price]
		df = df[df['from']>from_start]
		df = df[df['from']<from_end]
		print df.head(50)
		try:
			df = pd.merge(df, countries, on='city', how='left')
			print df.loc[df.groupby("city")["price_num"].idxmin()].head(20)
		except Exception, e:
			print(e)
		print df['price_num'].idxmin()
		print df.loc[df['price_num'].idxmin()]

	def optimum_weekends(self,duration_max=7,from_start='2020-01-01',allow_max_city_duplicates = 16,include_cities=[],exclude_cities=[],nr_random_itineraries=7000):
        df=pd.read_csv(self.csv_filename, encoding='utf-8')
        df['price_num'] = df['price'].str.replace(unicodedata.lookup('EURO SIGN'),'').apply(pd.to_numeric)
        df['duration_hour'] = df['duration'].str.extract(pat = '([0-9]*) h').apply(pd.to_numeric)
        df = df[df['duration_hour']<duration_max]
        df = df[df['from']>from_start]
        df = df[~df['city'].isin(exclude_cities)]
        current_min = 9999999
        for _ in range(nr_random_itineraries):
            df = df.sample(frac=1).reset_index(drop=True)
            res = df.groupby(['from','to']).first()
            if (res.shape[0] <= allow_max_city_duplicates+len(res.city.unique())):
                if (set(include_cities).issubset(set(res.city.unique()))):
                    if (current_min > res.price_num.sum()):
                        out = res
                        current_min = out.price_num.sum()
        try:
            print out
            print "Total budget: ",out.price_num.sum()
            print "Total different cities: ",len(out.city.unique())
            print "Total weekend trips: ",out.shape[0]
            return out
        except:
            print("Couldn't find a solution. Try increasing allow_max_city_duplicates, or setting include_cities=[] or exclude_cities=[].")

	def cheapest_price_per_location(self,location):
		df=pd.read_csv(self.csv_filename, encoding='utf-8')
		df['price_num'] = df['price'].str.replace(unicodedata.lookup('EURO SIGN'),'').apply(pd.to_numeric)
		df['duration_hour'] = df['duration'].str.extract(pat = '([0-9]*) h').apply(pd.to_numeric)
		df = df[df['city']==location]
		print df[df['price_num']==df['price_num'].min()]