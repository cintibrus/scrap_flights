# Scrapping Google flights

This python library uses Selenium to scrap Google flights. The scraper saves the data to a csv file that then can be used for multiple porpoises.

There are some examples of use here: 
[https://medium.com/@cintibrus/travel-budget-optimization-5c97501babe0](https://medium.com/@cintibrus/travel-budget-optimization-5c97501babe0)

To start using this library, first you will need to create the GoogleFlights object by specifying the name of the csv file to save.
```
gs = GoogleFlights(csv_filename)
```
Then you can call the method createDataWorldwide to scan all roundtrip flights from a city to any other city worldwide: 
```
gs.createDataWorldwide(travel_lenght_list,start_period,end_period,steps_days,takeoff_after,takeoff_until,return_after,return_until,max_budget,nr_passengers,city_code)
```
The arguments of the createDataWorldwide method are the following. 
The travel_lenght_list is an array with the number of nights you want to scan, e.g. [2,3] will scan trips with 2 and 3 nights. 
The start_period is the first departing date to start scanning; therefore, if you want to scan all roundtrips departing on Saturday and returning on Sunday make sure that start_period is a Saturday (e.g. '2020-01-04'), and travel_lenght_list=[1]. 
The end_period is the last departing date to scan; e.g. Dec. 30th 2020 ('2020-12-30'). 
The argument steps_days is how many days to jump between scanning departing dates; thus, if steps_days=7 it will scan weekly departure flights, and if steps_days=1 it will scan every day departure flights. 
One can use takeoff_after and takeoff_until to scan, for example, only trips that depart between 18 (takeoff_after='1800') and 22 hs (takeoff_until='2200'). 
Similarly, one can use return_after and return_until to scan flights that return, for example, between 17 (return_after='1700') and 24 hs (return_until='2400'). 
The argument max_budget will cap the maximum price in euros you are willing to pay when scanning for flights (e.g. max_budget=150).
One can specify the number of passengers with nr_passengers (e.g. nr_passengers=2).
Finally, the city code can be the IATA airport code or a city specific code for all airports in the city, and it represents the city from which we will depart and return to. Examples of such codes are: 
* Amsterdam: **/m/0k3p** for all (Schipol) airports; **AMS** Schipol airport;
* Paris: **/m/05qtj** for all airports; **ORY** Orly, **CDG** Charles de Gaulle, **BVA** Beauvais airports;
* Rome: **/m/06c62** for all airports; **CIA** Ciampino ...;
* London: **/m/04jpl** for all airports;  **LTN** Luton ...;

IATA airport codes can be found here: [https://www.iata.org/publications/pages/code-search.aspx](https://www.iata.org/publications/pages/code-search.aspx).
Google specific city codes can be obtained by changing the departure city in Google Flights website and exploring the URL it creates.

Default values are takeoff_after="0000",takeoff_until="2400",return_after="0000",return_until="2400",max_budget='300',nr_passengers=2, city_code='AMS'.

An example to scan every flight departing Friday after 18 hs., returning Sunday after 17 hs., for 2020, flying from Amsterdam to any destination in Europe, for 2 passengers, with a price lower than 300 euros in total, would look like:

`gs.createDataWorldwide([2],'2019-12-06','2020-12-30',7,takeoff_after='1800',return_after='1700',max_budget=300,nr_passengers=2,city_code='AMS')`
 
This method will create a csv file with all the destination options, departure and return dates, price, flight duration, and if there are any stops.

The head(6) for the example showed earlier looks like:

|city|duration|price|stops|from|to|
| ------ | ------ |------ |------ |------ |------ |
|London|1 h 10 m|€230|Non-stop|2019-12-06|2019-12-08|
|Valencia|2 h 25 m|€244|1 stop|2019-12-06|2019-12-08|
|Vienna|1 h 55 m|€292|Non-stop|2019-12-06|2019-12-08|
|Milan|1 h 35 m|€218|Non-stop|2019-12-06|2019-12-08|
|Frankfurt|1 h 5 m|€277|Non-stop|2019-12-06|2019-12-08|
|Manchester|1 h 25 m|€238|Non-stop|2019-12-06|2019-12-08|

Once the scraping ends, we can proceed to call the method optimum_weekends to get the cheapest travel itinerary for our selected dates:
```
gs.optimum_weekends(duration_max,from_start,allow_max_city_duplicates)
```
The argument duration_max filters out flights that are more than `x` hours long.
Argument from_start specifies a date to start the itinerary. 
The argument allow_max_city_duplicates filter out itineraries that have more than `y` amount of duplicate destinations.  
Default values are duration_max=7,from_start='2020-01-01',allow_max_city_duplicates = 16.

This method creates thousands of possible itineraries by selecting permutations of destinations for each date. 
The output is an itinerary with the lowest budget possible for the trips scanned. 
Note that because the minimum is find at the randomly created itineraries level, and there is a maximum toleration of duplicate cities, the cheapest itinerary is not the one that has the cheapest flight for each date. 
Finding the cheapest flight for each date will give trivial solutions; in the example above, will recommend to travel Amsterdam - London almost every weekend. 

If we call the optimum_weekends method for the example provided above, a subsample of the itinerary for Jan. and Feb. 2020, looks like:

|from|to|city|duration|stops|price|
| ------ | ------ |------ |------ |------ |------ |
|2020-01-04| 2020-01-05|       Milan|  1 h 45 m|  Non-stop|        296|
|2020-01-11| 2020-01-12|        Oslo|  1 h 50 m|  Non-stop|        251|
|2020-01-18| 2020-01-19|        Rome|  2 h 25 m|  Non-stop|        212|
|2020-01-25| 2020-01-26|  Copenhagen|  1 h 20 m|  Non-stop|        200|
|2020-02-01| 2020-02-02|   Marseille|  1 h 50 m|  Non-stop|        238|
|2020-02-08| 2020-02-09|  Birmingham|  1 h 15 m|  Non-stop|        250|
|2020-02-15| 2020-02-16|       Milan|  1 h 45 m|  Non-stop|        184|
|2020-02-22| 2020-02-23|      London|   1 h 0 m|  Non-stop|        138|
|2020-02-29| 2020-03-01|     Bristol|  1 h 20 m|  Non-stop|        119|

If instead of looking for an optimum itinerary, you are only interested in getting the cheapest dates to travel for an specific destination, you can call the method:
```
gs.cheapest_price_per_location(location)
```
where location is the destination you are interested in. 

For example, if you call `gs.cheapest_price_per_location('Edinburgh')` with the scanned data from the example above, the output will look like: 

|city|duration|price|stops|from|to|
| ------ | ------ |------ |------ |------ |------ |
|Edinburgh|  1 h 30 m|  €133|  Non-stop | 2020-06-06 | 2020-06-07|
|Edinburgh|  1 h 30 m|  €133|  Non-stop | 2020-08-08 | 2020-08-09|