from googleflights import *
from datetime import date
import pandas as pd

today = date.today().strftime("%Y-%m-%d")
steps_days = 7
end_period = '2020-12-01'

dd='friday'
if (dd=='friday'):
    print('---- Flights AMS - Anywhere Friday-Sunday ------')
    start_period = '2019-12-06'
    travel_lenght_list = [2]
    takeoff_after='1800'
    return_after = '1700'
if (dd=='thursday'):
    start_period = '2019-12-05'
    travel_lenght_list = [3]
    takeoff_after= '1900'
    return_after = '1700'
if (dd=='monday'):
    start_period = '2019-12-09'
    travel_lenght_list = [3]
    takeoff_after='0000'
    return_after = '1700'

print('## Scanning roundtrips from AMS to Europe')
city = 'AMS'
csv_filename='{city}_{dd}_{today}_{takeoff_after}_{return_after}.csv'.format(dd=dd,today=today,takeoff_after=takeoff_after,return_after=return_after)
gs = GoogleFlights(csv_filename)
gs.createDataWorldwide(travel_lenght_list,start_period,end_period,steps_days,takeoff_after=takeoff_after,return_after=return_after,max_budget=250,nr_passengers=1,city_code=city)
gs.cheapest_price_per_location('Edinburgh')
res = gs.optimum_weekends(duration_max=7,from_start='2020-01-01',allow_max_city_duplicates=0)

print('Optimum itinerary removing duplicates')
aux = res
aux = aux.sort_values(by=['price_num'])
aux.reset_index(inplace=True)
out = aux.groupby(['city']).first()
out.sort_values(by=['from'],inplace=True)
print out

print('## Scanning roundtrips from AMS to Asia country by country')
region='asia'
csv_filename='ams_asia_{dd}_{today}_{takeoff_after}_{return_after}.csv'.format(dd=dd,today=today,takeoff_after=takeoff_after,return_after=return_after)
gs = GoogleFlights(csv_filename)
gs.createDataFromCountry(region,travel_lenght_list,start_period,end_period,steps_days,takeoff_after=takeoff_after,return_after=return_after)
gs.analyseCountryData(max_price=13000,duration_max=50,stops_hour_max = 24,countries=df)

print('## Scanning roundtrips from AMS to a list of cities')
city_list=['FEZ','TUN','TLV','TFN','TFS','RAK','FNC','AAL','ABZ','AER','AGP','AKT','ALC','ARN','ATH','BCN','BEG','BFS','BGO','BGY','BHD','BHX','BLL','BLQ','BOD','BOH','BOJ','BOO','BRE','BRI','BRS','BRU','BSL','BTS','BUD','BZZ','CAG','CDG','CGN','CIA','CPH','CRL','CTA','CWL','DME','DRS','DSA','DTM','DUB','DUS','EDI','EIN','EMA','EXT','FAO','FCO','FFD','FKB','FMO','FRA','GDN','GLA','GOA','GOT','GRV','GVA','HAJ','HAM','HEL','HER','HRK','KBP','KEF','KJA','KRK','KTW','KUF','KZN','LBA','LED','LEJ','LGG','LGW','LHR','LIN','LIS','LJU','LKZ','LLA','LPA','LPL','LTN','LUX','LYS','MAD','MAN','MHZ','MLA','MMX','MRS','MSQ','MUC','MXP','NAP','NCE','NCL','NUE','NWI','ODS','OPO','ORK','ORY','OSL','OTP','PDL','PMI','PMO','POZ','PRG','PRN','PSA','RIX','RMS','ROV','SCQ','SIP','SJJ','SKG','SKP','SNN','SOF','SOU','SPC','STN','STR','SVG','SVO','SXF','TER','TFN','TFS','TGD','TIA','TLL','TLS','TOS','TRD','TRN','TSF','TXL','UFA','VAR','VCE','VIE','VKO','VNO','VRN','WAW','WMI','WRO','ZAG','ZIA','ZRH']
city_country=['Fes','Tunis','Tel Aviv','Tenerife North','Tenerife South','Morocco','Madeira','Denmark','United Kingdom','Russia','Spain','United Kingdom','Spain','Sweden','Greece','Spain','Serbia','United Kingdom','Norway','Italy','United Kingdom','United Kingdom','Denmark','Italy','France','United Kingdom','Bulgaria','Norway','Germany','Italy','United Kingdom','Belgium','France','Slovakia','Hungary','United Kingdom','Italy','France','Germany','Italy','Denmark','Belgium','Italy','United Kingdom','Russia','Germany','United Kingdom','Germany','Ireland','Germany','United Kingdom','The Netherlands','United Kingdom','United Kingdom','Portugal','Italy','United Kingdom','Germany','Germany','Germany','Poland','United Kingdom','Italy','Sweden','Russia','Switzerland','Germany','Germany','Finland','Greece','Ukraine','Ukraine','Iceland','Russia','Poland','Poland','Russia','Russia','United Kingdom','Russia','Germany','Belgium','United Kingdom','United Kingdom','Italy','Portugal','Slovenia','United Kingdom','Sweden','Spain','United Kingdom','United Kingdom','Luxembourg','France','Spain','United Kingdom','United Kingdom','Malta','Sweden','France','Belarus','Germany','Italy','Italy','France','United Kingdom','Germany','United Kingdom','Ukraine','Portugal','Ireland','France','Norway','Romania','Portugal','Spain','Italy','Poland','Czechia','Kosovo','Italy','Latvia','Germany','Russia','Spain','Ukraine','Bosnia and Herzegovina','Greece','Macedonia','Ireland','Bulgaria','United Kingdom','Spain','United Kingdom','Germany','Norway','Russia','Germany','Portugal','Spain','Spain','Montenegro','Albania','Estonia','France','Norway','Norway','Italy','Italy','Germany','Russia','Bulgaria','Italy','Austria','Russia','Lithuania','Italy','Poland','Poland','Poland','Croatia','Russia','Switzerland']
df_city_country = pd.DataFrame(list(zip(city_list, city_country)),columns =['city', 'country'])
csv_filename='ams_cities_{dd}_{today}_{takeoff_after}_{return_after}.csv'.format(dd=dd,today=today,takeoff_after=takeoff_after,return_after=return_after)
gs = GoogleFlights(csv_filename)
gs.createData(city_list,travel_lenght_list,start_period,end_period,steps_days,takeoff_after=takeoff_after,return_after=return_after)
gs.analyseData(max_price=130,duration_max=5,flight_hour_max = 24,countries=df_city_country)

print('## Scanning roundtrips from AMS to EZE from Oct. to Dec. 2019, any departure date, for 11, 12 or 13 nights')
start_period = '2019-10-10'
end_period = '2019-12-24'
travel_lenght_list = [13,12,11]
steps_days = 1
city_list=['EZE']
csv_filename='ams_eze_2019_{today}.csv'.format(today=today)
gs = GoogleFlights(csv_filename)
gs.createData(city_list,travel_lenght_list,start_period,end_period,steps_days)